const opts = {
    base: {
        demand: true,
        alias: 'b',
    },
    limit: {
        alias: 'l',
        default: 10,
    }
};

const argv = require('yargs')
    .command('listar', 'Muestra las tablas de multiplicar', opts)
    .command('crear','Genera un archivo con la tabla de multiplicar mediante especificación de una base',opts)
    .help()
    .argv;



module.exports = {
    argv
}