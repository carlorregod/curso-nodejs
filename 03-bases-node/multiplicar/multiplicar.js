//Para usar los filesystem se recomienda usar:
const fs = require('fs');
const colors = require('colors');
//Documentación espefífica: https://nodejs.org/docs/latest-v8.x/api/fs.html#fs_file_system

//Se creará un archivo que almacene la tabla de multiplicar. Documentación: fs.writeFile(file, data[, options], callback)
//https://nodejs.org/docs/latest-v8.x/api/fs.html#fs_fs_writefile_file_data_options_callback

//También se puede crear una exprtación directa así:
// module.exports.crearArchivo = ......
let crearArchivo = (base, limite=10) => {
    return new Promise((resolve, reject) => {

        if (!Number(base)) {
            reject('No es un número o no existe la base especificada');
            return;

        } else if(!Number(limite)) {
                reject('No existe o no es un numero el limite ingresado');
                return;
        } 
        else if(limite < 1 || base < 1) {
            reject('el limite o base ingresadas deben ser positivas, mayores o iguales a uno');
            return;
        } else {
            let data = '';
            for (let index = 1; index <= limite; index++) {
                data += `${base} * ${index} = ${base*index}\n`;
            }

            fs.writeFile('multiplicar/tabla_' + base + 'limite'+limite+'.txt', data, (err) => {
                if (err)
                    reject(err);
                else
                    resolve(`tabla_${base}- al -${limite}.txt`);
            });
        }
    });
};

let listarArchivo = (base, limite = 10) =>{

        if (!Number(base)) {
            console.log('No existe o no es un número el base ');
            return;
        } 
        else if(!Number(limite)) {
            console.log('No existe o no es un numero el limite ingresado');
            return;
        } 
        else if(limite < 1 || base < 1) {
            console.log('el limite o base ingresadas deben ser positivas, mayores o iguales a uno');
            return;
        }
        console.log('==================='.green);
        console.log(`====tabla de ${base}====`.red);
        console.log('==================='.green);
        var data = '';
        for (let i=1;i<=limite;i++)
        {
            console.log(`${base} * ${i} = ${base*i}`);
        }

    
}

//Para exportar funciones y otras cosas se usa la sentencia de module.
module.exports = {
    crearArchivo,
    listarArchivo,
}