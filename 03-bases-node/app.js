//const multiplicar = require('./multiplicar/multiplicar.js')
//Si se usa la destructuración se puede acceder rápidamente a la propiedad de module.extends que se vio antes
const { crearArchivo, listarArchivo } = require('./multiplicar/multiplicar.js');
const {argv} = require('./opciones/yargs');
const colors = require('colors');

// Otra forma de exportar
//const argv = require('./opciones/yargs').argv;

console.log(argv._); //Mostrará el array de opciones pasadas al argv
switch(argv._[0]) {
    case 'listar':
            listarArchivo(argv.base, argv.limit)
    break;

    case 'crear':
        crearArchivo(argv.base, argv.limit)
        .then(archivo => console.log(`Archivo creado: ${archivo.green}`))
        .catch(err => console.log(err));
    break;

    default:
        console.log('Opción no reconocida');
}


