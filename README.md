# Curso NodeJs

Estos son los apuntes para generar codigo fuente asociado para el aprendizaje de Node:

* NodeJs 8 o superior
* Nodemon

## Instalación

Sólo ejecutar git clone con este link

```
git clone https://gitlab.com/carlorregod/curso-nodejs.git
```

Se recomienda utilizar Visual Studio Code junto al plugin Live Action.

## Tecnologías

* Javascript EC6
* Postman

### ( ͡° ͜ʖ ͡°) ###

* ( ͡° ͜ʖ ͡°)
* ( ͡° ͜ʖ ͡°)
* ( ͡°( ͡° ͜ʖ( ͡° ͜ʖ ͡°)ʖ ͡°) ͡°)
* ( ͡° ͜ʖ ͡°)